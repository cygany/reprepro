This is the reprepro package from the salsa.debian.org, with our minor modifications
to become compilable under cygany, which is a debian overlay over cygwin.

Our cygany branch tracks "debian" in the salsa repository, roughly so:

Git remotes:
```
origin   git@gitlab.com:cygany/reprepro.git (fetch)
origin   git@gitlab.com:cygany/reprepro.git (push)
upstream https://salsa.debian.org/debian/reprepro.git (fetch)
upstream https://salsa.debian.org/debian/reprepro.git (push)
```

Branch setup:
```
* cygany c0deebe [origin/cygany] ...
  debian fce2ea3 [upstream/debian] ...
```

In our current state, it can be only compiled roughly so:

```
PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules build binary
```

But soon it will be hopefully better.
